package be.kdg.todo_app_demo.model

import kotlinx.serialization.Serializable

@Serializable
data class NewTodo(
    val userId: Int,
    val title: String,
    val completed: Boolean,
)