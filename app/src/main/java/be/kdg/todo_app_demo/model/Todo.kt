package be.kdg.todo_app_demo.model

import kotlinx.serialization.Serializable

@Serializable
data class Todo(
    val id: String,
    val userId: Int,
    val title: String,
    var completed: Boolean,
)