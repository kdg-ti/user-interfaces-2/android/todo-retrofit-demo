package be.kdg.todo_app_demo.network

import be.kdg.todo_app_demo.model.Todo
import be.kdg.todo_app_demo.model.NewTodo
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path


interface TodoApiService {
    @GET("todos")
    suspend fun getTodoList(): Response<List<Todo>>

    @POST("todos")
    suspend fun addTodo(
        @Body todo: NewTodo
    ): Response<Todo>

    @DELETE("todos/{id}")
    suspend fun deleteTodo(
        @Path("id") id: String
    ): Response<Unit>

    @PUT("todos/{id}")
    suspend fun setComplete(
        @Path("id") id: String,
        @Body todo: Todo
    ): Response<Todo>
}