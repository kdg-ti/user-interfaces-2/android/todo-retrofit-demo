package be.kdg.todo_app_demo.data

import be.kdg.todo_app_demo.model.Todo
import be.kdg.todo_app_demo.model.NewTodo
import be.kdg.todo_app_demo.network.TodoApiService
import retrofit2.HttpException
import java.io.IOException

interface TodoRepository {
    suspend fun getTodos(): List<Todo>
    suspend fun addTodo(todo: NewTodo): Todo?
    suspend fun deleteTodo(id: String): Boolean
    suspend fun setComplete(todo: Todo): Todo?
}

class NetworkTodoRepository(val todoApiService: TodoApiService) : TodoRepository {
    override suspend fun getTodos(): List<Todo> {
        try {
            val response = todoApiService.getTodoList()

            if (response.isSuccessful) {
                return response.body() ?: emptyList()
            } else {
                // Handle the error case
                throw HttpException(response)
            }
        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun addTodo(todo: NewTodo): Todo? {
        try {
            val response = todoApiService.addTodo(todo)
            if (response.isSuccessful) {
                return response.body()
            }
            // Handle the error case
            throw HttpException(response)

        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun deleteTodo(id: String): Boolean {
        try {
            val response = todoApiService.deleteTodo(id)

            return response.isSuccessful
        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }

    override suspend fun setComplete(todo: Todo): Todo? {
        try {
            val response = todoApiService.setComplete(todo.id, todo)
            if (response.isSuccessful) {
                return response.body()
            }
            // Handle the error case
            throw HttpException(response)

        } catch (e: IOException) {
            // Handle network I/O errors
            throw e
        } catch (e: HttpException) {
            // Handle HTTP errors
            throw e
        }
    }
}