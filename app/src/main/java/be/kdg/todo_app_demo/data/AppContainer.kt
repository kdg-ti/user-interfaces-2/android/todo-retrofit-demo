package be.kdg.todo_app_demo.data

import be.kdg.todo_app_demo.network.TodoApiService
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit

interface AppContainer {
    val todoRepository: TodoRepository
}

class DefaultAppContainer : AppContainer {
    private val BASE_URL =
        "http://10.0.2.2:3000/"

    private val json = Json {
        ignoreUnknownKeys = true
        coerceInputValues = true
    }

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
        .baseUrl(BASE_URL)
        .build()

    private val retrofitService: TodoApiService by lazy {
        retrofit.create(TodoApiService::class.java)
    }

    override val todoRepository: TodoRepository by lazy {
        NetworkTodoRepository(retrofitService)
    }
}