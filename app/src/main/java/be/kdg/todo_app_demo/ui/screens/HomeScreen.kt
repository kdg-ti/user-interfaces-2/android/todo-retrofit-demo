package be.kdg.todo_app_demo.ui.screens

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import be.kdg.todo_app_demo.R
import be.kdg.todo_app_demo.model.Todo


@Composable
fun HomeScreen(
    todoUiState: TodoUiState,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    onDelete: (Todo) -> Unit,
    onCheckedChange: (Todo) -> Unit,
) {
    when (todoUiState) {
        is TodoUiState.Error -> Text("Error")
        is TodoUiState.Loading -> Text(text = "Loading")
        is TodoUiState.Success -> ResultScreen(
            todoUiState.todos,
            onDelete,
            onCheckedChange,
            modifier.padding(top = contentPadding.calculateTopPadding())
        )
    }
}

/**
 * ResultScreen displaying number of photos retrieved.
 */
@Composable
fun ResultScreen(
    todos: List<Todo>,
    onDeleteClick: (Todo) -> Unit,
    onCompleteClick: (Todo) -> Unit,
    modifier: Modifier = Modifier
) {
    Box(
        contentAlignment = Alignment.Center, modifier = modifier
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(12.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            items(todos) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(4.dp)
                ) {
                    TodoItem(todo = it, onDeleteClick, onCompleteClick)
                }
            }
        }
    }
}

@Composable
fun TodoItem(
    todo: Todo,
    onDeleteClick: (Todo) -> Unit,
    onCompleteClick: (Todo) -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 14.dp)
    ) {
        Row(
            modifier = Modifier.padding(14.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {

            Text(
                modifier = Modifier.weight(5f),
                text = todo.title,
                fontSize = 20.sp,
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.Medium
            )

            Checkbox(
                modifier = Modifier.weight(1f),
                checked = todo.completed,
                onCheckedChange = { newState ->
                    Log.d("TAG", "changing to $newState")
                    onCompleteClick(todo)
                },
                colors = CheckboxDefaults.colors(
                    checkedColor = Color.Black, uncheckedColor = Color.Gray
                ),
                enabled = true
            )

            Icon(
                modifier = Modifier
                    .weight(1f)
                    .clickable { onDeleteClick(todo) },
                painter = painterResource(id = R.drawable.delete),
                contentDescription = null
            )
        }
    }
}


@Composable
@Preview
fun Preview3() {
    TodoItem(todo = Todo("1", 2, "this is my todo", true), {}, {})
}

@Composable
@Preview
fun ResultScreenPreview() {
    val todos = listOf<Todo>(
        Todo("1", 2, "this is my todo", true),
        Todo("1", 2, "this is my todo", true),
        Todo("1", 2, "this is my todo", true),
        Todo("1", 2, "this is my todo", true),
    )
    ResultScreen(todos, {}, {}, Modifier)
}
