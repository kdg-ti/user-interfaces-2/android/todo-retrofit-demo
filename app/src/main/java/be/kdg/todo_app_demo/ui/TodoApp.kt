@file:OptIn(ExperimentalMaterial3Api::class)

package be.kdg.todo_app_demo.ui

import android.widget.Toast
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import be.kdg.todo_app_demo.R
import be.kdg.todo_app_demo.model.NewTodo
import be.kdg.todo_app_demo.ui.screens.HomeScreen
import be.kdg.todo_app_demo.ui.screens.TodoUiState
import be.kdg.todo_app_demo.ui.screens.TodoViewModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoApp() {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    val todoViewModel: TodoViewModel = viewModel(factory = TodoViewModel.Factory)
    val snackbarHostState = remember { SnackbarHostState() }

    ToastMessage(todoViewModel)

    Scaffold(modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = { TodoTopAppBar(scrollBehavior = scrollBehavior) },
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        },
        floatingActionButton = {
            AddTodoFloatingActionButton(modifier = Modifier, onClick = {
                todoViewModel.addTodo(
                    NewTodo(2, "this is my todo", true),
                )
            })
        }) {
        Surface(
            modifier = Modifier.fillMaxSize()
        ) {
            HomeScreen(
                todoUiState = todoViewModel.todoUiState,
                onCheckedChange = { todo -> todoViewModel.toggleTodo(todo) },
                onDelete = { todo -> todoViewModel.deleteTodo(todo) },
                contentPadding = it,
            )
        }
    }
}

@Composable
private fun ToastMessage(todoViewModel: TodoViewModel) {
    // If there are user messages to show on the screen,
    // show it and notify the ViewModel.
    if (todoViewModel.todoUiState is TodoUiState.Success) {
        val currentState = todoViewModel.todoUiState as TodoUiState.Success
        currentState.userMessage?.let {
            val context = LocalContext.current
            LaunchedEffect(it) {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
                // Once the message is displayed and dismissed, notify the ViewModel.
                todoViewModel.userMessageShown()
            }
        }
    }
}

@Composable
fun AddTodoFloatingActionButton(modifier: Modifier, onClick: () -> Unit) {
    FloatingActionButton(
        modifier = modifier,
        containerColor = Color.Cyan,
        onClick = { onClick() },
    ) {
        Icon(Icons.Filled.Add, "Floating action button.")
    }
}

@Composable
fun TodoTopAppBar(scrollBehavior: TopAppBarScrollBehavior, modifier: Modifier = Modifier) {
    CenterAlignedTopAppBar(
        scrollBehavior = scrollBehavior, title = {
            Text(
                text = stringResource(R.string.todo_demo),
                style = MaterialTheme.typography.headlineSmall,
            )
        }, modifier = modifier
    )
}
