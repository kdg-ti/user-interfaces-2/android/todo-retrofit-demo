package be.kdg.todo_app_demo.ui.screens

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import be.kdg.todo_app_demo.TodoApplication
import be.kdg.todo_app_demo.data.TodoRepository
import be.kdg.todo_app_demo.model.Todo
import be.kdg.todo_app_demo.model.NewTodo
import kotlinx.coroutines.launch

sealed interface TodoUiState {
    object Loading : TodoUiState
    object Error : TodoUiState
    data class Success(
        val todos: List<Todo>,
        val userMessage: String? = null
    ) : TodoUiState
}

class TodoViewModel(private val todoRepository: TodoRepository) : ViewModel() {
    /** The mutable State that stores the status of the most recent request */
    var todoUiState: TodoUiState by mutableStateOf(TodoUiState.Loading)
        private set


    /**
     * Call getMarsPhotos() on init so we can display status immediately.
     */
    init {
        getTodos()
    }


    fun getTodos() {
        viewModelScope.launch {
            try {
                val listResult: List<Todo> = todoRepository.getTodos()

                Log.d("getTodos", "${listResult.size}")

                if (listResult.isEmpty()) {
                    todoUiState = TodoUiState.Error
                } else {
                    todoUiState =
                        TodoUiState.Success(todos = listResult)
                }
            } catch (e: Exception) {
                todoUiState = TodoUiState.Error
                Log.e("getTodos", "Error fetching todos", e)
            }
        }
    }

    fun userMessageShown() {
        if (todoUiState is TodoUiState.Success) {
            //Cast todoUiState to TodoUiState.Success to access todos.
            val currentState = todoUiState as TodoUiState.Success
            todoUiState = currentState.copy(userMessage = null)
        }
    }

    fun addTodo(todo: NewTodo) {
        viewModelScope.launch {
            try {
                val newTodo: Todo? = todoRepository.addTodo(todo)
                successMessage("New todo added id=${newTodo?.id}")
                getTodos()
            } catch (e: Exception) {
                todoUiState = TodoUiState.Error
                Log.e("addTodo", "Error adding todo", e)
            }
        }
    }

    private fun successMessage(message: String) {
        if (todoUiState is TodoUiState.Success) {
            //Cast todoUiState to TodoUiState.Success to access todos.
            val currentState = todoUiState as TodoUiState.Success
            todoUiState =
                TodoUiState.Success(
                    todos = currentState.todos,
                    userMessage = message
                )
        }
    }

    fun deleteTodo(todo: Todo) {
        viewModelScope.launch {
            val deleted = todoRepository.deleteTodo(todo.id)
            if (deleted) successMessage("Todo deleted id=${todo.id}")
            getTodos()
        }
    }

    fun toggleTodo(todo: Todo) {
        viewModelScope.launch {
            try {
                Log.e("toggle Todo", "Trying to update completed state")
                todo.completed = !todo.completed
                val newTodo: Todo? = todoRepository.setComplete(todo)
                successMessage("Todo changed to completed=${newTodo?.completed}")
                getTodos()
            } catch (e: Exception) {
                todoUiState = TodoUiState.Error
                Log.e("addTodo", "Error patching todo", e)
            }
        }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val application = (this[APPLICATION_KEY] as TodoApplication)
                val todoRepository = application.container.todoRepository
                TodoViewModel(todoRepository = todoRepository)
            }
        }
    }
}