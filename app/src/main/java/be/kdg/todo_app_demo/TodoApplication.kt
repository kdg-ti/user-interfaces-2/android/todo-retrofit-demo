package be.kdg.todo_app_demo

import android.app.Application
import be.kdg.todo_app_demo.data.AppContainer
import be.kdg.todo_app_demo.data.DefaultAppContainer

class TodoApplication : Application() {
    lateinit var container: AppContainer
    override fun onCreate() {
        super.onCreate()
        container = DefaultAppContainer()
    }

}