## Todo App Demo

Een basis web applicatie om een Todo web service aan te spreken

### Technologieën

- Jetpack Compose met viewmodel
- Retrofit 2 (with POST & PUT request)
- kotlinx serialization 
- json-server

#### start de json server lokaal op in de terminal

```cli
$ cd data
$ npm start
```

<img src="data/screenshots/screenshot.png" alt="Screenshot" width="270">
<img src="data/screenshots/screenshot-retrofit.jpg" alt="Screenshot" width="400">
